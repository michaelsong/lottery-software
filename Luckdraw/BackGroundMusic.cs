﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Text;
using System.Threading;

namespace Luckdraw
{
    class BackGroundMusic : ServiceBase
    {
        [DllImport("winmm.dll")]
        private static extern bool PlaySound(String Filename, int Mod, int Flags);


        //[DllImport("winmm.dll")]
        //public static extern long PlaySound(String fileName, long a, long b);


        [DllImport("winmm.dll")]
        public static extern long mciSendString(string lpstrCommand, string lpstrReturnString, long length, long hwndcallback);




        ///
        /// 播放音乐文件(重复)
        ///
        /// 音乐文件名称
        public static void PlayMusic_Repeat(string p_FileName)
        {
            try
            {
                mciSendString(@"close temp_music", " ", 0, 0);
                mciSendString(@"open " + p_FileName + " alias temp_music", " ", 0, 0);
                mciSendString(@"play temp_music repeat", " ", 0, 0);
            }
            catch
            { }
        }


        ///
        /// 播放音乐文件
        ///
        /// 音乐文件名称
        public static void PlayMusic(string p_FileName)
        {
            try
            {
                mciSendString(@"close temp_music", " ", 0, 0);
                mciSendString(@"open " + p_FileName + " alias temp_music", " ", 0, 0);
                mciSendString(@"play temp_music", " ", 0, 0);
            }
            catch
            { }
        }


        ///
        /// 停止当前音乐播放
        ///
        /// 音乐文件名称
        public static void StopMusic(string p_FileName)
        {
            try
            {
                mciSendString(@"close " + p_FileName, " ", 0, 0);
            }
            catch { }
        }






       

        protected override void OnStart(string[] args)
        {
            Thread thAlarm = new Thread(new ThreadStart(myStaticThreadMethod));
            thAlarm.SetApartmentState(ApartmentState.STA);
            thAlarm.Start();
        }


        private void myStaticThreadMethod()
        {
            //PlaySound(@"C:\Users\thinkpad\Desktop\新建文件夹\Classic\msg.wav", 0, 1); //把1替换成9，可连续播放 
            PlayMusic(@"C:\Users\thinkpad\Desktop\新建文件夹\Classic\msg.wav");
            //PlaySound(@"C:\Users\thinkpad\Desktop\新建文件夹\Classic\msg.wav", 0, 1); //把1替换成9，可连续播放 
        }


        protected override void OnStop()
        {
        }
    }
}